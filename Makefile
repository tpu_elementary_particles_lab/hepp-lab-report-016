BIBFILE=/home/samba/incoming/Books/bibtex_base.bib
LATEX=/usr/bin/xelatex
BIBTEX=/usr/bin/bibtex

CHAPTERS:=$(wildcard chapters/*.tex)
CONVERTED_CHAPTERS=$(patsubst %.tex,%_cp1251.tex,$(CHAPTERS))

# root.pdf
all: root.pdf #$(CONVERTED_CHAPTERS)

root.pdf: root.tex $(CHAPTERS)
	$(LATEX) $^
	$(BIBTEX) root
	$(LATEX) $^

recode: $(CONVERTED_CHAPTERS)

chapters/%_cp1251.tex: chapters/%.tex
	iconv -f utf8 -t cp1251 $^ > $@

.PHONY: all recode

